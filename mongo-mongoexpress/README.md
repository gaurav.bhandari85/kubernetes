## Some Notes
kubectl port-forward  --address 0.0.0.0  mongo-express-78fcf796b8-qflfx 9090:8081


### Reference
1. Port forward: https://phoenixnap.com/kb/kubectl-port-forward  
2. Demo Project: https://gitlab.com/nanuchi/youtube-tutorial-series/-/tree/master/demo-kubernetes-components  
