## Some Commands


### Connect Minikube with Docker
eval $(minikube docker-env)  
Note: Before building a docker image it is mandatory to execute above command before the image creation

### Create Docker build
docker build -t node-1 .
### Create a deployment and service
kubectl apply -f deployment.yml

### Port forward
kubectl port-forward  --address 0.0.0.0 <pod-name> 9090:5000  
example:  
kubectl port-forward  --address 0.0.0.0 hello-python-5749695896-28fcp 9090:5000  

